import { NextPage } from "next"


const Layout: NextPage<any> = (props: any) => {
    return(
        <>
        <main className="row">

        <div className="container">
            {props.children}
        </div>
        
        </main>
        </>
    )
}
export default Layout