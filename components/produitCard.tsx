import Link from "next/link"



export const ProduitCard = ({ produit }: any) => {

    return (

        <div>
            <div className="contenu justify-content-center col-6 d-flex">
                <div className=" category">
                    <Link href={'services/' + produit.id}>
                        <a className="linkDontDecorProduit" href={"/services/" + produit.id}>
                            {/* '/services/[id]' */}
                            <p className="img-produit-exemple">
                                {/* <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} /> */}
                                <div className="">
                                    <p className="">{produit.label}</p>
                                </div>
                            </p>
                        </a>
                    </Link>

                </div>
            </div>
        </div>
    )

}