import { NextPage } from "next"
import Image from "next/image"

const Footer: NextPage<any> = () => {
    return (
        <div className="justify-content-center align-item-center">
            <footer className="footer">
                <span className="reseau">&copy; 2017-2022 Company, Inc. &middot; <a className="reseau" href="#">Privacy</a> &middot; <a className="reseau" href="#">Terms</a></span>
            </footer>
        </div>
    )
}

export default Footer