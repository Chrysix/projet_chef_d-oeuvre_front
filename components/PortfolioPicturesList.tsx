import { PortfolioPictures } from "./PortfolioPictures"


export const PortfolioPicturesList = ({ list }: any) => {
    return (
        <>
            {list.map((item: any) => <PortfolioPictures key={item.id} portfolio={item} />)}
        </>

    )
}