import Image from "next/image"


export const PortfolioPictures = ({ portfolio }: any) => {
    return (
                <div className="col-md-3">
                    <img src={portfolio.image} width={300} height={450}/>
                    {/* layout="responsive" sizes="50vw" */}
                    <p>{portfolio.name}</p>
                    <p>{portfolio.description}</p>
                </div>
    )
}

