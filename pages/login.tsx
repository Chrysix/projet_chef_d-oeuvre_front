import { NextPage } from "next";
import { signIn, useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useState } from "react";
import { Button, Col, Form, InputGroup, Row } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";

interface SignIn {
    error: string | undefined;
    status: number;
    ok: boolean;
    url: string | null;
}

const Login: NextPage = () => {
    const { data, status } = useSession();
    const [error, setError] = useState<string | undefined>(undefined);
    const [showPass, setShowPass] = useState(false);
    const { register, handleSubmit, watch, control, formState } = useForm({
        defaultValues: {
            email: "",
            password: "",
        },
    });

    const router = useRouter();

    // const handleShowPass = () => {
    //     setShowPass(!showPass);
    // };

    const onSubmit = async (credentials: { email: string; password: string }) => {
        if (credentials.email && credentials.password) {
            const log = await signIn("signin", {
                redirect: false,
                password: credentials.password,
                email: credentials.email,
            }).then((result: SignIn | undefined) => {
                if (result !== undefined && result.error) {
                    setError(result.error);
                } else if (result !== undefined && result.url) {
                    router.push("/");
                }
            });
        }
    };
    return (
        <Row className="justify-content-center align-items-center w-100 mt-5 ms-0">
            <Col
                xs="11"
                md="6"
                className="signupAndIn justify-content-center py-4 rounded"
            >
                <h3 className="text-center mb-5">Connexion</h3>
                <Form
                    onSubmit={handleSubmit(onSubmit)}
                    className="d-flex flex-column align-items-center justify-content-center"
                >
                    <Col xs="12" md="8" lg="6">
                        <Form.Group controlId="formEmail">
                            <Form.Label>Email :</Form.Label>
                            <Controller
                                control={control}
                                name="email"
                                render={({ field: { onChange, onBlur, value, ref } }) => (
                                    <Form.Control
                                        onChange={onChange}
                                        value={value}
                                        ref={ref}
                                        autoComplete="email"
                                        isInvalid={
                                            formState.errors.email !== undefined ||
                                            error !== undefined
                                        }
                                        placeholder="Enter email"
                                        required
                                    />
                                )}
                            />
                            <Form.Control.Feedback type="invalid">
                                {formState.errors.email?.message}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="formPassword">
                            <Form.Label>Mot de passe :</Form.Label>
                            <InputGroup>
                                <Controller
                                    control={control}
                                    name="password"
                                    render={({ field: { onChange, onBlur, value, ref } }) => (
                                        <Form.Control
                                            type={showPass ? "text" : "password"}
                                            onChange={onChange}
                                            value={value}
                                            ref={ref}
                                            autoComplete="password"
                                            isInvalid={
                                                formState.errors.password !== undefined ||
                                                error !== undefined
                                            }
                                            placeholder="Enter password"
                                            required
                                        />
                                    )}
                                />
                                {/* <Button
                                    className="shadow-none"
                                    variant={showPass ? "secondary" : "outline-secondary"}
                                    id="eye-password"
                                    onClick={() => handleShowPass()}
                                >
                                    {showPass ? (
                                        <i className="bi bi-eye-slash-fill" />
                                    ) : (
                                        <i className="bi bi-eye-fill" />
                                    )}
                                </Button> */}
                            </InputGroup>
                            <Form.Control.Feedback type="invalid">
                                {formState.errors.password?.message}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Text className="text-danger">
                            {error !== undefined && <>Mauvais e-mail ou mot de passe</>}
                        </Form.Text>
                    </Col>
                    <Button type="submit" className="buttonConnect col-lg-4 col-4 mt-4">
                        Connexion
                    </Button>
                    <Form.Text muted className="mt-3 d-flex align-items-center">
                        Vous n'avez pas encore de compte ? {"-> "}Cliquez
                        <Button
                            // style={"color:blue;"}
                            size="sm"
                            variant="link"
                            onClick={() => router.push("/register")}
                        >
                            Ici
                        </Button>
                    </Form.Text>
                </Form>
            </Col>
        </Row>
    );
};

export default Login;

export async function getStaticProps(context: any) {
    return {
        props: {},
    };
}