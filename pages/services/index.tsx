import { GetServerSideProps, NextPage } from "next"
import { fetchCategorie, fetchProduit, fetchProduitId } from "../../utils/AxiosServices"
import Image from "next/image";
import inter from "../../utils/interceptor";
import { ReactChild, ReactFragment, ReactPortal, useState } from "react";
import Link from "next/link";
import { CategorieCard } from "../../components/CategorieCard";

const Services: NextPage = ({ produit, categorie }: any) => {

    const [prod, setProd] = useState(produit);
console.log(produit );
console.log(categorie);

    return (
        <div className="title">
            <h1>Services</h1>
            <div className="container">
                <div className="">
                <CategorieCard key={categorie.id} categorie={categorie} />
                </div>
            </div>
        </div>
    )


}
export default Services



export const getServerSideProps: GetServerSideProps = async () => {
    const PropCategory = await inter.get(process.env.NEXT_PUBLIC_API_URL + "/api/categorie");

    return {
        props: {
            produit: await fetchProduit(),
            categorie: await fetchCategorie()
        }
    }
}
