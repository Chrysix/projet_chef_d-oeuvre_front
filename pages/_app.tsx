import '.././styles/globals.css';
import '.././styles/style.css';
import '.././styles/Home.module.css';
import "bootstrap/dist/css/bootstrap.css";
import type { AppProps } from 'next/app';
import axios from 'axios';
import { SessionProvider } from 'next-auth/react';
import NavBar  from '../components/NavBar';
import Footer from '../components/Footer';
import '../utils/token';

axios.defaults.baseURL = 'http://localhost:3000'

function MyApp({ Component, pageProps }: AppProps) {
  if (typeof window !== "undefined") {
    // browser code
  }
  return (
    <SessionProvider session={pageProps.session}>
      <NavBar />
      <Component {...pageProps} />
      <Footer/>
    </SessionProvider>
  );
}
export default MyApp
