import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { GetServerSideProps, GetStaticProps, NextPage } from "next";
import Link from 'next/link';
import { useState } from 'react';
import axios from 'axios';
import {Col, Row } from 'react-bootstrap';

const Register: NextPage = () => {
    const [formData, setFormData] = useState({
        name: "",
        first_name: "",
        email: "",
        password: "",
        adress: "",
        code_postal: "",
        city: ""
    });


    const { name, first_name, email, password, adress, code_postal, city } = formData;

    const onChange = (e: any) =>
        setFormData({ ...formData, [e.target.name]: e.target.value });

    const onSubmit = async (e: any) => {
        e.preventDefault();

        //console.log(formData);
        const newUser = {
            name,
            first_name,
            email,
            password,
            adress,
            code_postal,
            city
        };
        try {
            const res = await axios.post("http://localhost:8000/api/user/register", newUser);
            console.log(res.data);
        } catch (error) {
            console.log(error);
        }
    }
    return (
        <>
            <Row className="justify-content-center align-items-center w-100 mt-5 ms-0">
                <Col
                    xs="11"
                    md="6"
                    className="signupAndIn justify-content-center py-4 rounded">
                    <h2 className="">S'inscrire</h2>
                    <form className="form-horizontal" onSubmit={(e) => onSubmit(e)} action="/action_page.php">
                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="Name">Name</label>
                            <div className="col-sm-10">
                                <input onChange={onChange} type="text" className="form-control" name="name" placeholder="Name" value={name} required />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="FirstName">First Name</label>
                            <div className="col-sm-10">
                                <input onChange={onChange} type="text" className="form-control" name="first_name" placeholder="FirstName" value={first_name} required />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="email">Email</label>
                            <div className="col-sm-10">
                                <input onChange={onChange} type="email" className="form-control" name="email" placeholder="Your email" value={email} required />
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input onChange={onChange} type="password" className="form-control" id="password" name="password" value={password} placeholder="Password" />
                        </div>
                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="Adresse">Adresse</label>
                            <div className="col-sm-10">
                                <input onChange={onChange} type="text" className="form-control" name="adress" placeholder="Adresse" value={adress} required />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="CodePostal">Code Postal</label>
                            <div className="col-sm-10">
                                <input onChange={onChange} type="number" className="form-control" name="code_postal" placeholder="CodePostal" value={code_postal} required />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="Ville">Ville</label>
                            <div className="col-sm-10">
                                <input onChange={onChange} type="text" className="form-control" name="city" placeholder="Ville" value={city} required />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-offset-2 col-sm-10">
                                <button type="submit" className="btn btn-dark" onClick={onSubmit} ><Link href="/">Register</Link> </button>
                            </div>
                        </div>
                    </form>
                </Col>
            </Row>
        </>
    )
}
export default Register