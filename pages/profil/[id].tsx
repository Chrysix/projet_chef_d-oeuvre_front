
import axios from "axios";
import { GetServerSideProps, GetStaticPaths, GetStaticProps, NextPage } from "next";
import { useSession } from "next-auth/react";
import Link from "next/link";
import { Col, Row } from "react-bootstrap";
import { Facture } from "../../components/Facture";
import { fetchUserId, fetchUser, fetchProduitId } from "../../utils/AxiosServices";

const Contact: NextPage = ({ utilisateur, produit, categorie }: any) => {
    const { data: session } = useSession();
    const user = session?.user as any;


    return (
        <div>
            <h1>Mon Profil</h1>
            
            <Row className="justify-content-center align-items-center w-100 mt-5 ms-0">
                <Col
                    xs="11"
                    md="6"
                    className="signupAndIn justify-content-center py-4 rounded">
                    {console.log(utilisateur)}

                    <h2>Mes informations : </h2>
                    <p>Nom : {utilisateur.name} <button>Modifier</button></p>
                    <p>Prénom : {utilisateur.first_name} <button>Modifier</button></p>
                    <p>Email : {utilisateur.email} <button>Modifier</button></p>
                    {/* <p>{utilisateur.password} <button>Modifier</button></p> */}
                    <p>Adresse : {utilisateur.adress} <button>Modifier</button></p>
                    <p>Code Postal : {utilisateur.code_postal} <button>Modifier</button></p>
                    <p>Ville : {utilisateur.city} <button>Modifier</button></p>

                    {/* <h2>Facture : </h2>



                    <div className="contenuService contenu">
                        <div className="">
                            <div className="">
                                <div className="facture">
                                    <Facture key={produit.id} facture={produit} />
                                </div>
                            </div>
                        </div>
                    </div> */}

                    <div className=" category">

                    </div>


                </Col>
            </Row>
        </div>
    )

}
export default Contact


export const getStaticPaths: GetStaticPaths = async () => {
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/user');
    const responseProd = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/produit');

    return {
        paths: response.data.map((item: any) => ({ params: { id: "" + item.id } })),
        fallback: false,
    };
};

export const getStaticProps: GetStaticProps = async (context) => {
    try {
        const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/user/' + context.params?.id);
        const responseProd = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/produit/' + context.params?.id);
        return {
            props: {
                utilisateur: response.data,
                produit: responseProd.data,
            },
        };
    } catch (e) {
        return {
            notFound: true,
        };
    }
};

{/*categorie.map((item: any) => {
    return(*/


            /* )
                })*/}