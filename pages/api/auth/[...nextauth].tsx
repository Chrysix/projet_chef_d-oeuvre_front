

































import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"
import axios from 'axios';
import { JWT } from "next-auth/jwt";
import { signOut } from "next-auth/react";


async function refreshAccessToken(token: JWT) {

	try {
		
		const response = await axios.post(process.env.NEXT_PUBLIC_API_URL + '/user/refreshToken', {}, {
			headers: {
				"Authorization": `bearer ${token.refresh_token}`
			},
		})


		return {
			...token,
			accessToken: response?.data.token,
			refreshToken: response?.data.refresh_token ?? token.refresh_token,
		}
	} catch (error) {
		//console.log(error)
		signOut()
		return {
			...token,
			error: "RefreshAccessTokenError",
		}
	}
}


export default NextAuth({
callbacks: {
		async jwt({ token, user }) {

			if (user !== undefined) {
				//@ts-ignore
				token.user = user?.user;
				//@ts-ignore
				token.accessToken = user?.token
				

			}

			return refreshAccessToken(token)
		},

		async session({ session, token }) {
			session.accessToken = token.accessToken
			//@ts-ignore
			session.user = token?.user
			session.refresh_token = token?.refresh_token
			return Promise.resolve(session);
		}
	},
	secret: process.env.JWT_SECRET,
	theme: {
		colorScheme: "light"
	},
	pages: {
		signIn: '/login',
		signOut: '/auth/signout',
	},


    providers: [
    CredentialsProvider({
        name: "Sign-In",
        type: "credentials",
        id: "signin",
        credentials: {
        email: { label: "Email", type: "text", placeholder: "Your Email..." },
        password: { label: "Password", type: "password", placeholder: "Your Password..." }
        },
        async authorize(credentials, req) {
        try {
        const response = await axios.post("http://localhost:8000/api/user/login", credentials);
        const data = response.data;
        if (data.user && data.token) {
            return data;

        }
        console.log("you are logged");

        return null;
        } catch (error) {
        console.log("error");
        return null;
        }
    }
    })
],
})






// import NextAuth from "next-auth"
// import CredentialsProvider from "next-auth/providers/credentials"
// import axios from 'axios';
// import { JWT } from "next-auth/jwt";
// import { signOut } from "next-auth/react";


// async function refreshAccessToken(token: JWT) {

// 	try {
		
// 		const response = await axios.post(process.env.NEXT_PUBLIC_API_URL + '/user/refreshToken', {}, {
// 			headers: {
// 				"Authorization": `bearer ${token.refresh_token}`
// 			},
// 		})


// 		return {
// 			...token,
// 			accessToken: response?.data.token,
// 			refreshToken: response?.data.refresh_token ?? token.refresh_token,
// 		}
// 	} catch (error) {
// 		console.log(error)
// 		signOut()
// 		return {
// 			...token,
// 			error: "RefreshAccessTokenError",
// 		}
// 	}
// }





// export default NextAuth({
//     callbacks: {
//         async jwt({ token, user }) {
//           if (user) {
//             return {
//               accessToken: user.token,
//               user: user.user,
//             }
//           }
    
//           return token;
//         },
//         async session({ session, token }) {
//           session.accessToken = token.accessToken;
//           session.user = token.user as any;
//           return session;
//         }
//       },
//   providers: [
//     CredentialsProvider({
//       name: "Sign-In",
//       type: "credentials",
//       id: "signin",
//       credentials: {
//         email: { label: "Email", type: "text", placeholder: "Your Email..." },
//         password: { label: "Password", type: "password", placeholder: "Your Password..." }
//       },
//       async authorize(credentials, req) {
//         try {
//           const response = await axios.post("http://localhost:8000/api/user/login", credentials);
//           const data = response.data;
//           if (data.user && data.token) {
//             return data;

//           }
//           console.log("you are logged");
          
//           return null;
//         } catch (error) {
//           console.log("error");
//           return null;
//         }
//       }
//     })
//   ],
// })