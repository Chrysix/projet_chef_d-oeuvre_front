
import { NextPage } from "next";
import { useSession } from "next-auth/react";
import Link from "next/link";
import { Col, Row } from "react-bootstrap";

const Contact: NextPage = () => {
    const { data: session } = useSession();
    const user = session?.user as any;

    return (
        <div>
            <h1>Contact</h1>
            {/* {session &&
            <p>you are logged in as {user.first_name}</p>
        } */}
            <Row className="justify-content-center align-items-center w-100 mt-5 ms-0">
                <Col
                    xs="11"
                    md="6"
                    className="signupAndIn justify-content-center py-4 rounded">
                    <form id="test-form" className="form-horizontal" action="/action_page.php">
                        <div className="form-group m-3">
                            <label className="control-label col-sm-2" htmlFor="Name">Nom : </label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" name="nom" id="nom" placeholder="Name" required />
                            </div>
                        </div>
                        <div className="form-group m-3">
                            <label className="control-label col-sm-2" htmlFor="FirstName">Prénom : </label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" name="prenom" id="prenom" placeholder="FirstName" required />
                            </div>
                        </div>
                        <div className="form-group m-3">
                            <label className="control-label col-sm-2" htmlFor="email">Email : </label>
                            <div className="col-sm-10">
                                <input type="email" className="form-control" name="email" id="email" placeholder="Your email" required />
                            </div>
                        </div>
                        <div className="form-group m-3">
                            <label className="control-label col-sm-2" htmlFor="numero">Numéro : </label>
                            <div className="col-sm-10">
                                <input type="number" className="form-control" name="numero" id="numero" placeholder="Your numero" required />
                            </div>
                        </div>
                        <div className="form-group m-3">
                            <label className="control-label col-sm-2" htmlFor="message">Votre message : </label>
                            <div className="col-sm-10">
                                <textarea className="form-control" name="message" id="message" placeholder="Your message" cols={30} rows={5}></textarea>
                            </div>
                        </div>
                        <div className="form-group m-3">
                            <div className="col-sm-offset-2 col-sm-10">
                                <button type="submit" id="submit-form" className="btn btn-dark" ><Link href="/">Envoyer</Link> </button>
                            </div>
                        </div>
                    </form>
                    

                </Col>
            </Row>
        </div>
    )

}
export default Contact