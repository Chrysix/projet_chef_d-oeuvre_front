import axios from "axios";
import { GetServerSideProps, GetStaticPaths, GetStaticProps } from "next";
import { useSession } from "next-auth/react";
import { redirect } from "next/dist/server/api-utils";
import { fetchProduit, ServiceOp } from "../../utils/AxiosServices";
import { BrowserRouter as Redirect } from "react-router-dom";
import { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useRouter } from "next/router";

const intialState = {
    label: '',
    matiere: '',
    type: ''

}

function categorie({ onFormSubmit, categorie, produit, paths }: any) {
    const router = useRouter();
    const [form, setForm] = useState(intialState);


    const handleChange = (event: { target: { name: any; value: any; }; }) => {
        const router = useRouter();
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }


    // const handleSubmit = (event: { preventDefault: () => void; }) => {

    //     event.preventDefault()
    //     onFormSubmit(form)

    // }

    const [operation, setOP]: any = useState([])
    const [label, setLabel]: any = useState([])
    const [matiere, setMatiere]: any = useState([])
    const [type, setType]: any = useState([])


    // const router = useRouter();
    // const { id } = router.query;

    const { data: session } = useSession()
    const user = session?.user as any;

    // async function addProduit(ope: any, e:any) {
    //     const response = await ServiceOp.addProduit(ope);
    //     setOP([
    //         ...operation,
    //         response.data
    //     ]);
    //     e.preventDefault();
    //     console.log(response);

    // }

    const handleSubmit = async (e: any) => {
        e.preventDefault();
        const facture = { label, matiere, type };
        await axios.post(process.env.NEXT_PUBLIC_SERVER_URL + "/api/produit", facture)
        // , {
        //     headers: { Authorization: `Bearer ${session?.accessToken}` },
        // });
        //router.push("/profile");
    };

    return (
        <div className="title">
            <h1>{categorie.label} </h1>
            <div className="container">
                <div className="row">
                    {/* <form onSubmit={addOP}> */}
                    <Form onSubmit={handleSubmit}>
                        <div className="contenu ">
                            <div className="mt-5">
                                <p>Selectionnez votre pièce</p>
                                <p>{}</p>
                                <div className=" col-6 d-flex">
                                    <>
                                        <Form.Select value={categorie.label} onChange={(e) => {
                                            setLabel(categorie.label);
                                        }}
                                            name="label" className="form-control">
                                            <option>Choisissez..</option>
                                            {categorie.produit.map((categorieId: any) => {
                                                return (
                                                    <option onChange={(e) => { handleChange }} value={categorieId.label}>{categorieId.label}</option>
                                                    // <option onChange={(e) => {
                                                    //     setLabel(categorieId.label);
                                                    // }} value={categorieId.label}>{categorieId.label}</option>
                                                )
                                            })}
                                        </Form.Select>
                                    </>
                                </div>
                            </div>

                            <div className="mt-5">
                                <p>Selectionnez la matière</p>
                                <div className=" col-6 d-flex">
                                    <>
                                        <Form.Select value={categorie.matiere} onChange={(e) => {
                                            setMatiere(categorie.matiere);
                                        }}
                                            name="matiere" className="form-control">
                                            <option>Choisissez..</option>
                                            {categorie.produit.map((categorieId: any) => {
                                                return (
                                                    <option onChange={(e) => { handleChange }} value={categorieId.matiere}>{categorieId.matiere}</option>
                                                    // <option onChange={(e) => {
                                                    //     setMatiere(categorieId.matiere);
                                                    // }} value={categorieId.matiere}>{categorieId.matiere}</option>
                                                )
                                            })}
                                        </Form.Select>

                                        {/* <Select placeholder="Remote" value={selectedId} onChange={e => setSelectedId(e)} 
                                        allowClear> 
                                        
                                        {remoteOption.map((item, i) => ( 
                                            <Select.Option value={item} key={i}> 
                                            {item} 
                                            </Select.Option> ))} 
                                        </Select> */}

                                    </>
                                </div>
                            </div>

                            <div className="mt-5">
                                <p>Que faut t-il faire dessus ?</p>
                                <div className=" col-6 d-flex">
                                    <>
                                        <Form.Select value={categorie.type} onChange={(e) => {
                                            setType(categorie.type);
                                        }}
                                            name="type" className="form-control">
                                            <option>Choisissez..</option>
                                            {categorie.produit.map((categorieId: any) => {
                                                return (
                                                    // <option value={categorieId.type}>{categorieId.type}</option>
                                                    <option onChange={(e) => { handleChange }} value={categorieId.type}>{categorieId.type}</option>
                                                    // <option onChange={(e) => {
                                                    //     setType(categorieId.type);
                                                    // }} value={categorieId.type}>{categorieId.type}</option>
                                                )
                                            })}
                                        </Form.Select>
                                    </>
                                </div>
                            </div>
                            <div>
                                {/* <p>{produit.prix}</p> */}
                            </div>

                        </div>

                        <Button className="btn-success" type="submit">Prenez rendez-vous →</Button>
                        {/* </form> */}
                    </Form>
                </div >
            </div >
        </div >
    )
}

export default categorie

export const getServerSideProps: GetServerSideProps = async (context) => {
    try {
        const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/categorie/' + context.params?.id);
        // const produitResponse = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/prodtui/');
        return {
            props: {
                categorie: response.data,
                produit: await fetchProduit(),
            },
        };
    } catch (e) {
        return {
            notFound: true,
        };
    }
};