import axios from "axios";
import { GetServerSideProps, NextPage } from "next";
import { Col, Row } from "react-bootstrap";
import { fetchCategorie, fetchProduit } from "../utils/AxiosServices";
import inter from "../utils/interceptor";

const Tarif: NextPage = ({ categorie, produit }: any) => {
    return (
        <div className='container'>
            <Row>
                <Col xs="12"
                    md="12"
                    className="signupAndIn justify-content-center py-4">
                    <h1 className="title">
                        Grille de Tarifs
                    </h1>
                    <div className="d-flex">
                        <div className="col-4">
                            <h3>Retouche : </h3>
                            <p>Trou à recoudre : 5 €</p>
                            <p>Remplacement ceinture éclair : 20 €</p>
                            <p>Remplacment bouton : 15 €</p>
                            <p>Ourlet à refaire : 30 €</p>
                            <p>Retaille à refaire : 35 €</p>
                            {/* {categorie.map((item: any) => {
                            return (
                                <div className="contenuService contenu">
                                    <h1 className="">{item.label}</h1>
                                </div>
                            )
                        })} */}
                        </div>

                        <div className="col-4">
                            <h3>Vêtement sur mesure : </h3>
                            <p>Pull : 20 €</p>
                            <p>Sweat : 25 €</p>
                            <p>Pantalon : 20 €</p>
                            <p>Robe : 70 €</p>
                            <p>Jupe : 15 €</p>
                            <p>Tee-shirt : 15 €</p>
                            <p>Polo : 20 €</p>
                            <p>Manteau : 80 €</p>
                            {/* {produit.map((produit: any) => {
                            return (
                                <p>{produit.label}</p>
                            )
                        })} */}
                        </div>

                        <div className="col-4">
                            <h3>Revêtement : </h3>
                            <p></p>
                            <p></p>
                            <p></p>
                            <p></p>
                            <p></p>

                        </div>




                        {/* <div>
                        {categorie.produit.map((categorieId: any) => {
                            return (
                                <p>{categorieId.label}</p>
                            )
                        })}
                        </div> */}

                    </div>
                </Col>
            </Row>
        </div>
    )
}
export default Tarif

export const getServerSideProps: GetServerSideProps = async (context) => {
    try {
        // const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/categorie/' + context.params?.id);
        // const produitResponse = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/prodtui/');
        return {
            props: {
                // categorie: response.data,
                // produit: await fetchProduit()
            },
        };
    } catch (e) {
        return {
            notFound: true,
        };
    }
};

