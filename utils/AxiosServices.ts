import axios from "axios";

//Portfolio

export async function fetchPortfolio() {
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/portfolio');

    return response.data
}

//User

export async function fetchUserId(id:number){
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/user/' + id);

        return response.data
}

export async function fetchUser(){
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/user/');

        return response.data
}

//Produit
export async function fetchProduit(){
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/produit');

        return response.data
}

export async function fetchProduitId(id:number){
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/produit' + id);

        return response.data
}

//Categorie
export async function fetchCategorie(){
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/categorie');

    return response.data
}

export async function fetchCategorie_Id(categorie_id:number){
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/produit' + categorie_id);

        return response.data
}


export class ServiceOp{

    static async add(ope: any) {
        console.log("ajouté");
        return axios.post(process.env.NEXT_PUBLIC_SERVER_URL+'/api/facture', ope);
        
    }

    static async addProduit(ope: any) {
        return axios.post(process.env.NEXT_PUBLIC_SERVER_URL+'/api/produit', ope);
    }
}
//Facture
// export async function fetchAddFacture(){
//     const response = await axios.post(process.env.NEXT_PUBLIC_SERVER_URL+'/api/facture');
//         return response.data
// }


// export async function fetchPortfolioId(id:number) {
//   const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/portfolio/'+ id);
//  
//     return response.data
// }