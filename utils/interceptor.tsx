import axios from "axios";
import { getSession } from "next-auth/react";

const inter = axios.create({
	baseURL: process.env.NEXT_PUBLIC_API_URL
})
inter.interceptors.response.use((response) => {

	return response
}, (error) => {
	
	console.log(error);
})
inter.interceptors.request.use(async (config) => {
	
	const session = await getSession();
	if (session) {

		config.headers.authorization = 'bearer ' + session.accessToken;
	}
	return config;
});



export default inter
